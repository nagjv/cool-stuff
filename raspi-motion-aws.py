from gpiozero import MotionSensor
from picamera import PiCamera
import datetime
from time import sleep
from fractions import Fraction
import threading
from time import sleep
import boto3

def upload_file_to_s3(file_name):
        print("#### upload to s3 #### {}".format(file_name))
        s3 = boto3.resource('s3')
        data = open(file_name, 'rb')
        s3.Bucket('actual-images').put_object(Key=file_name, Body=data)
        print("#### completed upload to s3 #### {}".format(file_name))

def start_motion_capture():
        pir = MotionSensor(4)
        cam = PiCamera()
        cam.resolution = (1024, 768)
        sleep(3)
        while True:
                pir.wait_for_motion()
                print("*** motion capture start ***")
                file_name = datetime.datetime.today().strftime("%H%M%S-%a%d%b%y").lower()
                file_name += ".h264"
                cam.brightness = 60
                cam.start_recording(file_name)
                sleep(2) #2 seconds video recording
                cam.stop_recording()
                pir.wait_for_no_motion()
                print("* motion capture end ***")
                threading.Thread(target=upload_file_to_s3, args=[file_name]).start()


if __name__ == '__main__':
        start_motion_capture()
